README
------
This is the README file from the PARSEME verbal multiword expressions (VMWEs) corpus for English, edition 1.3. See the wiki pages of the [PARSEME corpora initiative](https://gitlab.com/parseme/corpora/-/wikis/home) for the full documentation of the annotation principles.

The present English data result from an enhancement of the English part of the [PARSEME corpus v 1.1](http://hdl.handle.net/11372/LRT-2842).
For the changes with respect to version 1.1, see the change log below.

Corpora
-------
All annotated data come from one of the following Universal Dependencies English treebanks (http://universaldependencies.org/):
1. `UD Original`: 4,221 sentences from the English Web Treebank.
2. `UD LinES`: 3,015 sentences from the English half of the LinES Parallel Treebank
3. `UD PUD`: 201 sentences from the English portion of the Parallel Universal Dependencies treebanks


Format
--------------------
The data are in the [.cupt](http://multiword.sourceforge.net/cupt-format) format. Here is detailed information about some columns:
* For most columns: Manual annotation or automatic annotation with manual correction. The UD tagsets (probably from an early release of the UD version 2) are used. For more details - see the treebank reference.
* PARSEME:MWE (column 11): Manually annotated by a single annotator per file. The following [VMWE categories](http://parsemefr.lif.univ-mrs.fr/parseme-st-guidelines/1.3/?page=030_Categories_of_VMWEs) are annotated: VID, LVC.full, LVC.cause, VPC.full, VPC.semi, IAV, MVC.

Statistics
-------
To know the number of annotated VMWEs of different types and with different properties (length, continuity, etc.), use these scripts: [mwe-stats.py](https://gitlab.com/parseme/utilities/-/blob/master/st-organizers/corpus-statistics/mwe-stats.py) and [mwe-stats-simple.py](https://gitlab.com/parseme/utilities/-/blob/master/st-organizers/corpus-statistics/mwe-stats-simple.py). 

Licences
-------
The UD Original and UD LinES corpora are licensed under **Creative Commons Share-Alike 4.0** licence [CC-BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/)
The UD PUD corpus is licensed under **Creative Commons Share-Alike 3.0** licence [CC-BY-SA 3.0](https://creativecommons.org/licenses/by-sa/3.0/)
The VMWE database is licensed under **Creative Commons 4.0** licence [CC-BY 4.0](https://creativecommons.org/licenses/by-sa/4.0/)


Authors
-------
The original VMWE annotations were performed by Claire Bonial, Kristina Geeraert, John McCrae, Nathan Schneider, Clarissa Somers, Abigail Walsh for edition 1.1. Consistency checks of these annotations were done by Tom Pickard and Alexandra Butler.


Reference
--------
Abigail Walsh, Claire Bonial, Kristina Geeraert, John P. McCrae, Nathan Schneider, and Clarissa Somers. 2018. [Constructing an Annotated Corpus of Verbal MWEs for English](https://aclanthology.org/W18-4921/). In Proceedings of the Joint Workshop on Linguistic Annotation, Multiword Expressions and Constructions (LAW-MWE-CxG-2018), pages 193–200, Santa Fe, New Mexico, USA. Association for Computational Linguistics.

Contact
-------
* Tom Pickard <tmrpickard1@sheffield.ac.uk>
* Alexandra Butler <agbutler@ucdavis.edu>
* Abigail Walsh <abigail.walsh@adaptcentre.ie>

Change log
----------
- **2023-04-15**:
  - Version 1.3 of the corpus was released on LINDAT.
  - Changes with respect to the 1.1 version results from [consistency checks](https://gitlab.com/parseme/corpora/-/wikis/Enhancing-existing-corpora#how-to-ensure-the-consistency-of-annotations) performed by Tom Pickard and Alexandra Butler
- **2018-04-30**:
  - [Version 1.1](http://hdl.handle.net/11372/LRT-2842) of the corpus was released on LINDAT.
